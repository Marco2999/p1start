package edu.uprm.cse.datastructures.cardealer.util;

import java.util.Iterator;

import edu.uprm.cse.datastructures.cardealer.util.CircularSortedDoublyLinkedList.Node;

public class IteratorL<E> implements Iterator<E>{

	Node<E> temp;

	public IteratorL(Node<E> head){ // Returns an iterator object of the LinkedList starting with the Header
		temp= head;
	}

	@Override
	public boolean hasNext() { // Detects if the current Node has a next node
		if(temp.getNext() != null) {
			return true;
		}		
		return false;
	}

	@Override
	public E next() { // Returns the next node in the LinkedList in accordance to the current node 
		if(hasNext()) {
			temp = temp.getNext();
			return temp.getElement();
		}
		else {
		return null;
		}
		
	}
}
