package edu.uprm.cse.datastructures.cardealer;

import edu.uprm.cse.datastructures.cardealer.model.Car;
import edu.uprm.cse.datastructures.cardealer.util.CircularSortedDoublyLinkedList;

public class CarList {
	
	public static final CircularSortedDoublyLinkedList<Car> cList = new CircularSortedDoublyLinkedList<Car>(new CarComparator<Car>());
	public CarList() {
		
	}
	
	public static CircularSortedDoublyLinkedList<Car> getInstance() { // Returns an instance of the car list
		
		return cList;
	}

	public static void resetCars() { // Resets the car list back to empty
		
		cList.clear();	
		
	}

}                                
