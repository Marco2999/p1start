package edu.uprm.cse.datastructures.cardealer;

import java.util.Comparator;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import edu.uprm.cse.datastructures.cardealer.model.Car;
import edu.uprm.cse.datastructures.cardealer.util.CircularSortedDoublyLinkedList;

@Path ("/cars")
public class CarManager {

	private final CircularSortedDoublyLinkedList<Car> carList = CarList.getInstance();

	@GET
	@Produces(MediaType.APPLICATION_JSON)	
	public Car[] getAllCars() { // Returns all cars in the list
		if(carList.isEmpty())
			return null;

		Car[] result = new Car[carList.size()];
		for(int i = 0; i < carList.size(); i++) {
			result[i] = carList.get(i);
		}
		return result;
	}


	@GET 
	@Path ("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Car getCar(@PathParam("id") long id) { // Finds and returns car using the car's ID
		for(int i = 0; i < carList.size(); i++) {
			if(carList.get(i).getCarId() == id) {
				return carList.get(i); 
			}
		}
		 throw new NotFoundException();
	}

	@POST
	@Path("/add")
	@Produces(MediaType.APPLICATION_JSON) 
	public Response addCar(Car c) {
		carList.add(c);
		return Response.status(201).build();
	}

	@PUT
	@Path("/{id}/update")
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateCar(Car car) { 				// Deletes and adds the same car to update it
		for(int i = 0; i < carList.size(); i++) {       // Returns OK if car found and updated, returns 404 if car not found

			if(carList.get(i).getCarId() == car.getCarId()) {
				carList.remove(i);
				carList.add(car);

				return Response.status(Response.Status.OK).build();
			}
		}
		return Response.status(404).build();
	}

	@DELETE
	@Path("/{id}/delete")
	public Response removeCar(@PathParam("id") long id) { //Finds and removes car using its ID
		for(int i = 0; i < carList.size(); i++) {         //Returns OK if car found and removed, returns 404 if car not found
			if(carList.get(i).getCarId() == id) {
				carList.remove(i);
				return Response.status(Response.Status.OK).build();
			}
		}
		return Response.status(404).build();
	}

	public void resetCars() {

		carList.clear();

	}



}
