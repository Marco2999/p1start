package edu.uprm.cse.datastructures.cardealer;

import java.util.Comparator;

import edu.uprm.cse.datastructures.cardealer.model.Car;

public class CarComparator<E> implements Comparator<E> {

	@Override
	public int compare(E o1, E o2) { // Object comparator for comparing cars on its brand, model, and model option
		Car car1 = (Car) o1;
		Car car2 = (Car) o2;
		
		String s1 = car1.getCarBrand() + car1.getCarModel() + car1.getCarModelOption();
		String s2 = car2.getCarBrand() + car2.getCarModel() + car2.getCarModelOption();
		
	
		return s1.compareTo(s2);
	}

}
