package edu.uprm.cse.datastructures.cardealer.util;

import java.lang.reflect.Array;
import java.util.Comparator;
import java.util.Iterator;

import edu.uprm.cse.datastructures.cardealer.CarComparator;
import edu.uprm.cse.datastructures.cardealer.model.Car;

public class CircularSortedDoublyLinkedList<E> implements SortedList<E>{

	public static class Node<E> { // Nodes for the links. Is linked to the next and
		private E element;        // previous nodes in the linked list
		private Node<E> next;
		private Node<E> previous;

		public Node(E element, Node<E> next, Node<E> previous) {
			super();
			this.element = element;
			this.next = next;
			this.previous = previous;
		}

		public Node() {
			super();
		}

		public E getElement() {
			return element;
		}
		public void setElement(E element) {
			this.element = element;
		}
		public Node<E> getNext() {
			return next;
		}
		public Node<E> getPrevious() {
			return previous;
		}
		public void setNext(Node<E> next) {
			this.next = next;
		}
		public void setPrevious(Node<E> previous) {
			this.previous = previous;
		}


	}

	private Node<E> header;
	private int currentSize;

	private Comparator<E> com;


	public CircularSortedDoublyLinkedList(Comparator<E> comparator) { // Comparator object for the LinkedList

		header = new Node<E>(null, header, header);
		this.currentSize = 0;
		this.com = comparator;

	}

	@Override
	public Iterator<E> iterator() { // Iterator object to iterate through the LinkedList

		return new IteratorL<E>(this.header);

	}

	@SuppressWarnings("unchecked")
	@Override
	public boolean add(E obj) { // Adds a node to the LinkedList in a sorted fashion

		if(this.size()==0) {
			
			Node<E> target = new Node<E>(obj, header, header);
			header.setNext(target);
			header.setPrevious(target);
			currentSize++;
			return true;
			
		} 
		else { 
			Node<E> tem = this.header; 
			while(tem.getNext() !=header) {
				tem = tem.getNext();
				if(this.com.compare(obj, tem.getElement())<=-1) {
					Node<E> target = new Node<E>(obj,tem,tem.getPrevious());
					tem.getPrevious().setNext(target);
					tem.setPrevious(target);
					this.currentSize++;
					return true;
				}
				if(tem.getNext() == this.header) {
					Node<E> target = new Node<E>(obj,this.header,this.header.getPrevious());
					this.header.getPrevious().setNext(target);
					this.header.setPrevious(target);
					this.currentSize++;
					return true;
				}
				

			}
			return false;
		}
	}

	@Override
	public int size() { // Returns size int
		return this.currentSize;
	}

	@Override
	public boolean remove(E obj) { // Removes an object in the LinkedList 
		if(this.contains(obj)) {
			return this.remove(this.firstIndex(obj));
		}
		return false;
	}

	@Override
	public boolean remove(int index) { // Removes an object in the LinkedList in the appointed index parameter
		if((index<0) || (index>this.currentSize)) {
			throw new IndexOutOfBoundsException();
		}

		if(!this.isEmpty()) {
			int i = 0;
			Node<E> temp = this.header.getNext();
			while(i != index) {
				temp = temp.getNext();
				i++;
			}
			this.currentSize--;
			temp.getPrevious().setNext(temp.getNext());
			temp.getNext().setPrevious(temp.getPrevious());
			temp.setElement(null); temp.setNext(null); temp.setPrevious(null);
			return true;	
		}
		return false;
	}

	@Override
	public int removeAll(E obj) { // Removes all copies of the Object obj found in the LinkedList
		int i = 0;             
		while(this.remove(obj)) {
			i++;
		}
		return i;	
	}

	@Override
	public E first() { // Returns the first node's element
		return this.header.getNext().getElement();
	}

	@Override
	public E last() { // Returns the last node's element
		return this.header.getPrevious().getElement();
	}

	@Override
	public E get(int index) { // Finds and returns the object in the specified index
		int c = 0;
		Node<E> temp = this.header.getNext();
		while(c!=index) {
			c++;
			temp = temp.getNext();
		}
		return temp.getElement();
	}

	@Override
	public void clear() { // Wipes the LinkedList back to empty
		while(!this.isEmpty()) {
			this.remove(0);
		}
	}

	@Override
	public boolean contains(E e) { // Returns true if the element is found in the LinkedList using it's first index
		return firstIndex(e) >= 0;
	}

	@Override
	public boolean isEmpty() { // Returns true if the LinkedList has no nodes in it
		return this.currentSize == 0;
	}

	@Override
	public int firstIndex(E e) { // Returns the int value of the first instance of the Object e in the LinkedList
		int pos = 0;

		for(Node<E> temp = this.header.getNext();temp != this.header; temp = temp.getNext()) {
			if(temp.getElement().equals(e)) {
				return pos;
			}
			pos++;
		}
		return -1;
	}

	@Override
	public int lastIndex(E e) { // Returns the int value of the last instance of the Object e in the LinkedList
		int pos = this.currentSize-1;

		for(Node<E> temp = this.header.getPrevious();temp != this.header; temp = temp.getPrevious()) {
			if(temp.getElement().equals(e)) {
				return pos;
			}
			pos--;
		}
		return -1;
	}

	public E[] toArray(E[] e) { // Produces an ArrayList copy of the current LinkedList
		E[] nList = (E[]) new Object[this.currentSize];
		
		for(int i = 0; i <this.currentSize; i++) {
			nList[i] = this.get(i);
		}
		return nList;
		
	}


}
